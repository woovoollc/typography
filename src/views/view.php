<div class="styles">
    <div class="styles-group">
        <div class="styles-header dropped" onclick="toggleLayout(this)">
            <span>Typography</span>
        </div>

        <div id="styles-layout">
            <div class="styles-option kit-field">
                <div class="kit-group">
                    <span>Font-size:</span>
                </div>
                <div class="kit-group">
                    <input type="text" id="font-size" class="kit-input"
                           onchange="window.setStyle('font-size', this.value)">
                </div>
                <div class="kit-group">
                    <span>Color:</span>
                </div>
                <div class="kit-group">
                    <input type="text" id="color" class="kit-input"
                           onchange="window.setStyle('color', this.value)">
                </div>
                <div class="kit-group">
                    <span>Line-height:</span>
                </div>
                <div class="kit-group">
                    <input type="text" id="color" class="kit-input"
                           onchange="window.setStyle('color', this.value)">
                </div>
            </div>

            <div class="styles-option">
                <span>Weight:</span>
                <ul class="styles-css" id="font-weight">
                    <li data-value="normal" onclick="setFontWeight(this, 'normal')">
                        Normal
                    </li>
                    <li data-value="italic"
                        onclick="setFontWeight(this, 'italic')">
                        Italic
                    </li>
                    <li data-value="bold"
                        onclick="setFontWeight(this, 'bold')">
                        Bold
                    </li>
                    <li data-value="lighter" onclick="setFontWeight(this, 'lighter')">
                        Lighter
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
    function setFontWeight(el, value) {
        if (el.classList.contains('styles-selected')) {
            el.classList.remove('styles-selected');
        } else {
            el.classList.add('styles-selected');
        }

        window.setStyle('font-weight', value);
    }

    function toggleLayout(el) {
        if (el.classList.contains('dropped')) {
            el.classList.remove('dropped');
            document.getElementById('styles-layout').style.display = 'none';
        } else {
            el.classList.add('dropped');
            document.getElementById('styles-layout').style.display = 'block';
        }
    }
</script>