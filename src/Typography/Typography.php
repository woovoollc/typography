<?php

namespace Typography;

use Illuminate\Support\Facades\Facade;

class Typography extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'typography';
    }

}