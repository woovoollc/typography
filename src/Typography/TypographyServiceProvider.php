<?php

namespace Typography;

use Illuminate\Support\ServiceProvider;

class TypographyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('typography', function () {
            return new Typography();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'typography');

        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('vendor/typography.php'),
            __DIR__ . '/../views' => resource_path('views/vendor/typography')
        ], 'typography');
    }
}